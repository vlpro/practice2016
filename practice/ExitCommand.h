#pragma once
#include "Command.h"

namespace Practice 
{
	struct ExitCommand : Command
	{
		void exec() 
		{
			cout << " <��������� �����������>" << endl << endl;
			exit(0);
		}
	};
}