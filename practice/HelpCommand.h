﻿#pragma once
#include "Command.h"

namespace Practice 
{
	using namespace std;

	struct HelpCommand : Command 
	{
		void exec() 
		{
			cout << " Список команд:                                                  " << endl << endl;
			cout << "   gen   - Генерирование и заполнение массива(ов) (Туровец)      " << endl;
			cout << "   del   - Удалить файл(ы)                                       " << endl;
			cout << "   exit  - Выход из консоли                                      " << endl << endl;
			cout << "   ran   - Диапазон      (Голубков)                              "	<< endl;
			cout << "   max   - Максимум      (Еременко)                              "	<< endl;
			cout << "   avr   - Среднее       (Ильченко)                              "	<< endl;
			cout << "   div   - Разнообразие  (Петухов)                               "	<< endl;
			cout << "   min   - Минимум       (Смирнов)                               "	<< endl;
			cout << "   sum   - Сумма         (Тощиков)                               "	<< endl;
			cout << "   maj   - Большинство   (Черкасова)                             " << endl;
			cout << "   dev   - Отклонение    (Щеколдин)                              " << endl << endl;
		}
	};
}