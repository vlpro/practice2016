#pragma once
#include "Matrix.h"

namespace Practice 
{
	using namespace std;

	class Matrices
	{
	public:
		int width;
		int height;
		vector<MatrixInt> matrices;
		Matrices(vector<MatrixInt> m, int w, int h) : matrices(m), width(w), height(h) {}
	};
}