#pragma once
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdio.h>

namespace Practice 
{
	using namespace std;

	template<class T>
	struct Matrix
	{
		Matrix(vector<vector<T>> d, int w, int h) : data(d), width(w), height(h) {}

		vector<vector<T>> data = {};

		unsigned int width  = 0;   // ������ �������
		unsigned int height = 0;   // ����� �������

		T get(int i, int j) 
		{
			return data[i][j];
		}

		bool isValid() 
		{
			if (data.size() != height)
			return false;

			for (int i = 0; i < height; i++) 
			{
				auto line = data[i];
				if (line.size() != width)
				return false;
			}
			return true;
		}

		int strWidth(T x)
		{
			return toStr(x).size();
		}

		string toStr(T data) 
		{
			std::ostringstream oss;
			oss << data;
			return oss.str();
		}

		string toString() 
		{
			stringstream buffer;

			if (!isValid()) 
			{
				buffer << " Matrix not valid " << data.size() << " h:" << height << " w:" << width;
				return buffer.str();
			}

			int columnWidth = 0;


			for (int i = 0; i < height; i++) 
			{
				for (int j = 0; j < width; j++) 
				{
					int num = strWidth(get(i, j));
					if (columnWidth < num) 
					{
						columnWidth = num;
					}
				}
			}

			for (int i = 0; i < height; i++) 
			{
				for (int j = 0; j < width; j++) 
				{
					buffer << setw(columnWidth + 1) << get(i, j);
				}
				buffer << endl;
			}

			return buffer.str();
		}

		void show() 
		{
			cout << toString();
		}

	};

	typedef Matrix<int> MatrixInt;

	int* readIntFromStringStream(istringstream stream) {
		int* number = nullptr;
		if (!(stream >> *number)) {
			return nullptr;
		}
		return number;
	}

	MatrixInt* readMatrixFromFile(string filename)
	{
		ifstream inputFile(filename);
		MatrixInt* result = nullptr;
		if (inputFile.is_open())
		{
			int rows, cols;
			inputFile >> rows;
			inputFile >> cols;
			
			if (rows < 0 || cols < 0)
				return nullptr;

			vector<vector<int>> matrixVector(rows);
			for (int i = 0; i < rows; i++)
				matrixVector[i] = vector<int>(cols);
			
			int number = 0;

			for (int i = 0 ; i < rows; i++)
				for (int j = 0; j < cols; j++)
				{
					if (!(inputFile >> number))
						return nullptr;
					
					matrixVector[i][j] = number;
				}


			result = new Matrix<int>(matrixVector, cols, rows);
		}
		return result;
	}
}