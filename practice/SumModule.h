#pragma once
#include "IntModule.h"

namespace Practice 
{
	struct SumModule : IntModule 
	{
		void help()
		{
			cout << "  <�����>" << endl << endl;
			cout << " ��������: �������, ������������ ��� ������� ���������  "          << endl;
			cout << "           ����� �������� �� ������� �������      "                << endl << endl;
		}

		int calc(InputData data) 
		{
			int sum = 0;

			for (int i = 0; i < data.size(); i++) 
			{
				sum += data[i];
			}

			return sum;
		}
	};
}
                                                                             