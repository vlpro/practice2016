﻿#pragma once
#include "Command.h"

namespace Practice 
{
	struct DeleteCommand : Command
	{
		inline bool fileExists(const std::string& name) 
		{
			struct stat buffer;
			return (stat(name.c_str(), &buffer) == 0);
		}

		void exec() 
		{
			int i = 0;

			while (fileExists(to_string(i + 1) + ".txt")) 
			{
				i++;
			}

			cout << " --------------------------------------------------------------- " << endl;
			cout << "  <Удаление файлов>" << endl << endl;
			cout << " Удалить " << i << " файлов?" << endl;

			if (confirm()) 
			{
				deleteFiles(i);
			}
		}

		void deleteFiles(int count) 
		{
			bool err = false;

			for (int i = 0; i < count; i++) 
			{
				if (remove((to_string(i + 1) + ".txt").c_str()) != 0) 
				{
					err = true;
					cout << " Ошибка при удалении файла. " << (i + 1) << ".txt";
				}
			}

			if (!err) 
			{
				cout << " Файлы успешно удалены." << endl;
			}
		}
	};
}