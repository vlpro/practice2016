#pragma once
#include "Command.h"

namespace Practice 
{
	template<class Output>
	struct Module : Command 
	{
		typedef vector<int> InputData;


		virtual void help() = 0;

		virtual Output calc(InputData data) = 0;

		inline bool fileExists(const std::string& name)
		{
			struct stat buffer;
			return (stat(name.c_str(), &buffer) == 0);
		}

		vector<MatrixInt> specifyMatrices()
		{
			vector<MatrixInt> matrices;
			cout << "[1]: ������� ������� 1.txt, 2.txt, ..." << endl;
			cout << "[2]: ������� ������� �������" << endl;
			int menuItem = readIntFromConsole();
			int w = 0, h = 0;
			int oldW = 0, oldH = 0;
			if (menuItem == 1)
			{
				int i = 0;
				int number;

				string filename = to_string(i + 1) + ".txt";
				
				while (fileExists(filename))
				{

					auto matrixFromFile = readMatrixFromFile(filename);

					if (matrixFromFile == nullptr)
					{
						cout << "���� �� ���������" << endl;
						break;
					}

					MatrixInt matrix = *matrixFromFile;

					if (i > 0 && (oldW != w || oldH != h))
					{
						cout << " ������: ������� ������ ������������" << endl;
						matrices.clear();
						break;
					}

					if (matrix.isValid())
					{
						matrices.push_back(matrix);
						oldW = w;
						oldH = h;
					}
					else
					{
						cout << " ������ �" << (i + 1) << " �����������" << endl;
					}

					i++;
					filename = to_string(i + 1) + ".txt";
				}
			} else
			if (menuItem == 2)
			{
				cout << "���������� ������: ";
				int numberOfMatrices = readIntFromConsole();
				int i = 0;
				string filename;
				clean_stdin();
				int oldW = 0;
				int oldH = 0;
				while (i < numberOfMatrices) 
				{
					cout << "������� ���� �� �����:" << endl;
					getline(cin, filename);
					if (filename == ":q") {
						matrices.clear();
						return matrices;
					}
					if (fileExists(filename))
					{
						auto matrixFromFile = readMatrixFromFile(filename);
						if (matrixFromFile != nullptr)
						{
							if (i > 0 && (oldH != matrixFromFile->height || oldW != matrixFromFile->width)) {
								cout << " ������: ������� ������ ������������" << endl;
								matrices.clear();
								return matrices;
							}
							else {
								oldH = matrixFromFile->height;
								oldW = matrixFromFile->width;
							}
							matrixFromFile->show();
							matrices.push_back(*matrixFromFile);
							i++;
						} else {
							cout << "������� �� ���������";
						}
					}
				}
			}
			cout << " --------------------------------------------------------------- " << endl;
			cout << endl;
			
			return matrices;
		}

		void exec()
		{
			help();

			auto matrices = specifyMatrices();

			if (matrices.size() != 0) {

				int height = matrices[0].height;
				int width = matrices[0].width;

				vector<vector<Output>> matrixVector(height);
				for (int i = 0; i < height; i++)
					matrixVector[i] = vector<Output>(width);

				vector<int> params(matrices.size());
				
					for (int i = 0; i < height; i++)
					{
						for (int j = 0; j < width; j++)
						{
							for (int k = 0; k < matrices.size(); k++)
							{
								params[k] = matrices[k].get(i, j);
							}
							matrixVector[i][j] = calc(params);
						}
					}
				

				Matrix<Output> matrix(matrixVector, width, height);

				matrix.show();
			} else {
				cout << "������� �� ������" << endl;
			}
			cout << endl;
		}
	};
}