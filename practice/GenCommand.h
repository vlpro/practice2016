﻿#pragma once
#include "Command.h"
#include "CurrentData.h"

namespace Practice 
{
	using namespace std;

	struct GenCommand : Command
	{


		int stroka, 
			kol, 
			stolb,
			min,
			max;

		void exec() 
		{

			cout << endl;
			cout << " Введите необходимое кол-во строк: ";
			stroka = readIntFromConsole();

			cout << " Введите необходимое кол-во столбцов: ";
			stolb = readIntFromConsole();

			cout << " Минимальное значение элемента: ";
			min = readIntFromConsole();

			cout << " Максимальное значение элемента: ";
			max = readIntFromConsole();

			if (min > max)
			{
				cout << endl;
				cout << " Ошибка: Минимальное число должно быть < макс." << endl << endl;
				clean_stdin();
				return;
			}

			cout << " Введите необходимое кол-во файлов: ";
			kol = readIntFromConsole();
			
			cout << endl;

			if (confirm()) 
			{
				generate();
			}
		}


		int randBetween(int from, int to) 
		{
			return rand() % (to - from + 1) + from;
		}

		void generate() 
		{
			vector<vector<int>> arr(stroka);
			for (int i = 0; i < stroka; i++)
				arr[i] = vector<int>(stolb);
			string str;
			ofstream fout;

			unsigned rand_value = 11;
			srand(rand_value);

			for (int i = 0; i < kol; i++)
			{

				str = to_string(i + 1);
				fout.open(((str)+".txt"));

				fout << stroka << endl;
				fout << stolb << endl;

				for (int i = 0; i < stroka; i++)
				{
					for (int j = 0; j < stolb; j++)
					{
						arr[i][j] = randBetween(min, max);
						fout << arr[i][j] << '\t';
						cout << '\t' << arr[i][j];
					}
					cout << endl;
					fout << endl;
				}
				cout << endl;
				fout.close();
			}

			cout << " Генерация завершена." << endl;
		}

	};
}