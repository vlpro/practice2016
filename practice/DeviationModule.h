#pragma once
#include "DoubleModule.h"

namespace Practice
{
	struct DeviationModule : DoubleModule
	{
		void help()
		{
			cout << "  <����������� ������������������ ����������>"                     << endl << endl;
			cout << " ��������: �������, ������������ ��� ������� ���������  "          << endl;
			cout << "           ����������� ���������� �������� �� ������� �������"     << endl << endl;
		}

		double calc(InputData data)
		{
			double medium = 0.0, Deviation = 0.0;

			for (int i = 0; i < data.size(); i++) 
			{
				medium += data[i];
			}

			medium = medium / data.size();

			for (int i = 0; i < data.size(); i++) 
			{
				Deviation += pow((data[i] - medium), 2);
			}

			return sqrt(Deviation / data.size());
		}
	};

}