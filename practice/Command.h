﻿#pragma once
#include <string>
#include <iostream>

namespace Practice 
{
	using namespace std;

	struct Command 
	{
		virtual ~Command() {};
		virtual void exec() = 0;


		void clean_stdin()
		{
			while (getchar() != '\n');
		}


		int readIntFromConsole()
		{
			int result;

			while (!(cin >> result))
			{
				cin.clear();
				clean_stdin();
				cout << "Number incorrect. Please try again:\n";
			}

			return result;
		}

		bool confirm() 
		{
			clean_stdin();
			string input;
			while (true)
			{
				cout << " Подтверждение (Y - да, N - нет): ";
				getline(cin, input);

				cout << endl;
				if (input == "Y" || input == "y" || input == "")
				return true;

				else if (input == "N" || input == "n") 
				{
					cout << " Отменено." << endl;
					cout << " --------------------------------------------- " << endl;
					return false;
				}
			}
		}
	};
}